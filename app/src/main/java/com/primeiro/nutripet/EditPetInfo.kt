package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_edit_pet_info.*
import kotlinx.android.synthetic.main.activity_edit_substitute_food.*

class EditPetInfo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_pet_info)

        val manage_edit_pet_data = DataPet(applicationContext)
        val petID = intent.getIntExtra("PET_ID", 0)

        var init_name = manage_edit_pet_data.RecoverNamePet().toString().split(",").toMutableList()
        var init_animal = manage_edit_pet_data.RecoverTypeAnimal().toString().split(",").toMutableList()
        var init_race = manage_edit_pet_data.RecoverRace().toString().split(",").toMutableList()
        var init_weight = manage_edit_pet_data.RecoverWeight().toString().split(",").toMutableList()
        var init_age = manage_edit_pet_data.RecoverAge().toString().split(",").toMutableList()

        text_edit_pet_name.setText(init_name[petID + 1])
        text_edit_animal.setText(init_animal[petID + 1])
        text_edit_race.setText(init_race[petID + 1])
        text_edit_weight.setText(init_weight[petID + 1])
        text_edit_age.setText(init_age[petID + 1])

        button_edit_pet.setOnClickListener{
            var name11 = manage_edit_pet_data.RecoverNamePet().toString().split(",").toMutableList()
            var animal11 = manage_edit_pet_data.RecoverTypeAnimal().toString().split(",").toMutableList()
            var race11 = manage_edit_pet_data.RecoverRace().toString().split(",").toMutableList()
            var weight11 = manage_edit_pet_data.RecoverWeight().toString().split(",").toMutableList()
            var age11 = manage_edit_pet_data.RecoverAge().toString().split(",").toMutableList()

            if (text_edit_pet_name.text.toString() == "" || text_edit_pet_name.text.toString().contains(",")) {
                Toast.makeText(this, "Digite o nome do pet e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (text_edit_animal.text.toString() == "" || text_edit_animal.text.toString().contains(",")) {
                Toast.makeText(this, "Digite um tipo de animal e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (text_edit_race.text.toString() == "" || text_edit_race.text.toString().contains(",")) {
                Toast.makeText(this, "Digite uma raça e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (text_edit_weight.text.toString() == "") {
                Toast.makeText(this, "Digite um peso", Toast.LENGTH_SHORT).show()
            } else if (text_edit_age.text.toString() == "") {
                Toast.makeText(this, "Digite uma idade", Toast.LENGTH_SHORT).show()
            } else {
                name11[petID + 1] = text_edit_pet_name.text.toString()
                animal11[petID + 1] = text_edit_animal.text.toString()
                race11[petID + 1] = text_edit_race.text.toString()
                weight11[petID + 1] = text_edit_weight.text.toString()
                age11[petID + 1] = text_edit_age.text.toString()

                var name = name11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
                var animal = animal11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
                var race = race11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
                var weight = weight11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
                var age = age11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")

                manage_edit_pet_data.SaveNamePet("$name")
                manage_edit_pet_data.SaveTypeAnimal("$animal")
                manage_edit_pet_data.SaveRace("$race")
                manage_edit_pet_data.SaveWeight("$weight")
                manage_edit_pet_data.SaveAge("$age")

                Toast.makeText(this, "Cadastro do Pet atualizado com sucesso!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewPets::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onBackPressed() {
        val petID = intent.getIntExtra("PET_ID", 0)
        var intent = Intent(this, ViewPetsInfo::class.java)
        intent.putExtra("position", petID)
        startActivity(intent)
        finish()
    }
}