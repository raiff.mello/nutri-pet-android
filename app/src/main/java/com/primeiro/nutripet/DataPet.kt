package com.primeiro.nutripet

import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast

class DataPet(private val context: Context) {

    private val ARCHIVEPET = "data.pet"

    private val ID_CLASS = "ID"
    private val ID_PET = "ID_PET"
    private val NAMEPET = "NAME"
    private val ANIMAL = "ANIMAL"
    private val RACE = "RACE"
    private val WEIGHT = "WEIGHT"
    private val AGE = "AGE"
    private val preferences: SharedPreferences
    private val editor: SharedPreferences.Editor


    fun IncrementIdClass(){
        editor.putInt(ID_CLASS, RecoverIdClass() + 1)
        editor.commit()
    }

    fun SaveIdPet(data: String?){
        editor.putString(ID_PET, data)
        editor.commit()
    }

    fun SaveNamePet(data: String?){
        editor.putString(NAMEPET, data)
        editor.commit()
    }

    fun SaveTypeAnimal(data: String?){
        editor.putString(ANIMAL, data)
        editor.commit()
    }

    fun SaveRace(data: String?){
        editor.putString(RACE, data)
        editor.commit()
    }

    fun SaveWeight(data: String?){
        editor.putString(WEIGHT, data)
        editor.commit()
    }

    fun SaveAge(data: String?){
        editor.putString(AGE, data)
        editor.commit()
    }

    fun RecoverIdClass(): Int {
        return preferences.getInt(ID_CLASS, 0)
    }
    fun RecoverIdPet(): String? {
        return preferences.getString(ID_PET, "")
    }
    fun RecoverNamePet(): String? {
        return preferences.getString(NAMEPET, "")
    }
    fun RecoverTypeAnimal(): String? {
        return preferences.getString(ANIMAL, "")
    }
    fun RecoverRace(): String? {
        return preferences.getString(RACE, "")
    }
    fun RecoverWeight(): String? {
        return preferences.getString(WEIGHT, "")
    }
    fun RecoverAge(): String? {
        return preferences.getString(AGE, "")
    }

    init {
        preferences = context.getSharedPreferences(ARCHIVEPET, 0)
        editor = preferences.edit()
    }

    fun DeletePetRegistration(index: Int): Boolean {
        var name11 = RecoverNamePet().toString().split(",").toMutableList()
        var animal11 = RecoverTypeAnimal().toString().split(",").toMutableList()
        var race11 = RecoverRace().toString().split(",").toMutableList()
        var weight11 = RecoverWeight().toString().split(",").toMutableList()
        var age11 = RecoverAge().toString().split(",").toMutableList()
        var id11 = RecoverIdPet().toString().split(",").toMutableList()

        name11.removeAt(index)
        animal11.removeAt(index)
        race11.removeAt(index)
        weight11.removeAt(index)
        age11.removeAt(index)
        id11.removeAt(index)

        var name = name11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
        var animal = animal11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
        var race = race11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
        var weight = weight11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
        var age = age11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
        var id = id11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")

        SaveNamePet("$name")
        SaveTypeAnimal("$animal")
        SaveRace("$race")
        SaveWeight("$weight")
        SaveAge("$age")
        SaveIdPet("$id")
        Toast.makeText(context, "Cadastro do Pet Apagado com sucesso!", Toast.LENGTH_SHORT).show()
        return true
    }

    fun DeleteAllPets(){
        SaveNamePet("")
        SaveTypeAnimal("")
        SaveRace("")
        SaveWeight("")
        SaveAge("")
        SaveIdPet("")
    }

    data class ViewPetData(
            val name: String,
            val animal: String,
            val race: String,
            val weight: String,
            val age: String,
            val photographpet: Int)

    class ViewPetDataBuilder{
        var name: String = ""
        var animal: String = ""
        var race: String = ""
        var weight: String = ""
        var age: String = ""
        var photographpet: Int = 0


        fun build():ViewPetData = ViewPetData(name, animal, race, weight, age, photographpet)
    }

    fun viewPetData(block: ViewPetDataBuilder.() -> Unit): ViewPetData = ViewPetDataBuilder().apply(block).build()

    fun addViewPetData(viewPetData: ViewPetData, retorno:MutableList<ViewPetData>): MutableList<ViewPetData> {
        var mut = retorno
        mut.add(viewPetData)
        return mut
    }

}