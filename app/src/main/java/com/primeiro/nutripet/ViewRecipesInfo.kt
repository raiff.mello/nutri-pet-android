package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_view_owner.*
import kotlinx.android.synthetic.main.activity_view_pets_info.*
import kotlinx.android.synthetic.main.activity_view_pets_info.bt_back
import kotlinx.android.synthetic.main.activity_view_recipes_info.*
import kotlinx.android.synthetic.main.activity_view_substitute_food.*
import java.io.File


class ViewRecipesInfo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_recipes_info)

        intent.extras?.let {
            val position = it.getInt("position")
            val recipes = RecipeData(applicationContext)

            var name1 = recipes.RecoverNameRecipe().toString().split(",").toMutableList()
            var ingredients1 = recipes.RecoverIngredients().toString().split(",").toMutableList()
            var preparation_mode1 = recipes.RecoverPreparationMode().toString().split(",").toMutableList()

            view_recipe_name.text = name1[position+1]
            view_ingredients.text = "Ingredientes:\n" + ingredients1[position+1]
            view_preparation_mode.text = "Modo de Preparo:\n" + preparation_mode1[position+1]

            bt_delete_recipe.setOnClickListener {
                val builder = AlertDialog.Builder(this)
                builder.setMessage("Realmente desejas deletar a receita ?")
                        .setCancelable(false)
                        .setPositiveButton("Sim") { dialog, id ->
                            var intent = Intent(this, ViewRecipes::class.java)
                            startActivity(intent)
                            finish()
                            recipes.DeleteRecipeRegistration(position + 1)
                        }
                        .setNegativeButton("Não") { dialog, id ->
                            dialog.dismiss()
                        }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }

            bt_back_recipes.setOnClickListener {
                var intent = Intent(this, ViewRecipes::class.java)
                startActivity(intent)
                finish()
            }

            button_edit_recipes_info.setOnClickListener {
                var intent = Intent(this, EditRecipes::class.java)
                intent.putExtra("RECIPE_ID", position)
                startActivity(intent)
                finish()
            }
        }
    }


    override fun onBackPressed() {
        var intent = Intent(this, ViewRecipes::class.java)
        startActivity(intent)
        finish()
    }
}