package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.primeiro.nutripet.Adapters.*
import kotlinx.android.synthetic.main.activity_view_recipes.*


class ViewRecipes : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_recipes)

        val recipes = RecipeData(applicationContext)
        var intent = Intent(this, ViewRecipesInfo::class.java)
        var retorno:MutableList<RecipeData.ViewRecipeData> = mutableListOf()
        var name1 = recipes.RecoverNameRecipe().toString().split(",").toMutableList()

        if (name1.size == 1){
            empty1.setText("Parece que não tem nenhuma receita aqui!")
        }else{
            empty1.setText("")
            for (i in 1..name1.count()-1){
                 var view = recipes.viewRecipeData {
                    if (name1[i].length>=17){
                        name = "Receita: " + name1.get(i).substring(0..17) + "..."
                    }else {
                        name = "Receita: " + name1.get(i)
                    }
                }
                retorno = recipes.addViewRecipeData(view, retorno)
            }

            recycler_recipes.adapter = ViewRecipeDataAdapter(retorno)
            recycler_recipes.layoutManager = LinearLayoutManager(this)

            recycler_recipes.addOnItemClickListenerRecipe(object: OnItemClickListenerRecipe {
                override fun onItemClickedRecipe(position: Int, view: View) {
                    for (ii in 0..retorno.size)
                        if (position == ii){
                            intent.putExtra("position", position)
                            startActivity(intent)
                            finish()
                        }
                }
            })
        }

        bt_back_menu_recipes.setOnClickListener {
            var intent = Intent(this, InitMenuScreen::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflate = menuInflater
        inflate.inflate(R.menu.menu_list_pets_recipes, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.add_pet_recipes){
            var intent = Intent(this, RecipesRegistrationScreen::class.java)
            startActivity(intent)
            finish()
        }
        return true
    }

    override fun onBackPressed() {
        var intent = Intent(this, InitMenuScreen::class.java)
        startActivity(intent)
        finish()
    }
}