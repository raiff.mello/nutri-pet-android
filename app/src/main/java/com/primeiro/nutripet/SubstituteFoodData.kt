package com.primeiro.nutripet

import android.content.Context
import android.content.SharedPreferences
import java.io.File

class SubstituteFoodData(private val context: Context, private val petID: String?) {
    private val SUBFOODFILE = "data.substituteFood"         // Local archive
    private val LUNCHSUBFOOD = "LUNCHSUBFOOD"               // Key to substitute food lunch
    private val DINNERSUBFOOD = "DINNERSUBFOOD"             // Key to substitute food dinner
    private val STATUSSUBSTITUTEFOOD = "register"           // Control variable

    private val preferences: SharedPreferences
    private val editor: SharedPreferences.Editor


    fun SaveSubstituteFoodLunch(data: String?){
        // Function to save the substitute food lunch in the local file.
        editor.putString(LUNCHSUBFOOD, data)
        editor.commit()
    }
    fun SaveSubstituteFoodDinner(data: String?){
        // Function to save the substitute food dinner in the local file.
        editor.putString(DINNERSUBFOOD, data)
        editor.commit()
    }

    fun RetrieveSubstituteFoodLunch(): String? {
        // Function that returns the substitute food lunch saved in the local file.
        return preferences.getString(LUNCHSUBFOOD, "")
    }

    fun RetrieveSubstituteFoodDinner(): String? {
        // Function that returns the substitute food dinner saved in the local file.
        return preferences.getString(DINNERSUBFOOD, "")
    }

    fun ChangeStatusSubstituteFood(substitute_food__init: String?) {
        // Function responsible for modifying the substitute food control variable.
        editor.putString(STATUSSUBSTITUTEFOOD, substitute_food__init)
        editor.commit()
    }

    fun DeleteSubstituteFood() {
        // Function responsible for deleting the substitute food data saved in the local file.
        preferences.edit().clear().commit()
        ChangeStatusSubstituteFood("register")
        File("/data/data/com.primeiro.nutripet/shared_prefs/data.substituteFood" + petID + ".xml").delete()
    }

    init {              // Function that initializes sharedpreferences
        preferences = context.getSharedPreferences(SUBFOODFILE + petID, 0)
        editor = preferences.edit()
    }

}