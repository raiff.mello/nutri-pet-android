package com.primeiro.nutripet.Alarm

import android.content.Intent
import android.os.IBinder
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.Observer
import com.primeiro.nutripet.Data.Alarm
import com.primeiro.nutripet.Data.AlarmRepository


class RescheduleAlarmsService : LifecycleService() {
    override fun onCreate() {
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        deleteAlarmsNotStarted()
        val alarmRepository = AlarmRepository(application)

        alarmRepository.getAlarmsLiveData().observe(this,
            Observer<List<Alarm>> { alarms ->
                for (a in alarms) {
                    if (a.isStarted) {
                        a.schedule(applicationContext)
                    }
                }
            })
        return START_STICKY
    }

    fun deleteAlarmsNotStarted() {
        val alarmRepository = AlarmRepository(application)
        alarmRepository.getAlarmsLiveData()
                .observe(this, Observer { alarms ->
                    for (alarm in alarms) {
                        if(!alarm.isStarted) {
                            alarmRepository.delete(alarm)
                        }
                    }
                })
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        super.onBind(intent)
        return null
    }
}
