package com.primeiro.nutripet.Alarm

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import android.os.Vibrator
import androidx.annotation.Nullable
import androidx.core.app.NotificationCompat
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.TITLE
import com.primeiro.nutripet.CreateAlarm.NotificationChannel.Companion.CHANNEL_ID
import com.primeiro.nutripet.RingActivity
import com.primeiro.nutripet.R


class AlarmService : Service() {
    private var mediaPlayer: MediaPlayer? = null
    private var vibrator: Vibrator? = null
    override fun onCreate() {
        super.onCreate()
        mediaPlayer = MediaPlayer.create(this, R.raw.alarm)
        mediaPlayer!!.isLooping = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val notificationIntent = Intent(this, RingActivity::class.java)
        notificationIntent.putExtra(TITLE, intent.getStringExtra(TITLE))
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmTitle = String.format("Alarme - %s", intent.getStringExtra(TITLE))
        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(alarmTitle)
            .setContentText("Ring Ring .. Ring Ring")
            .setSmallIcon(R.drawable.ic_alarm)
            .setContentIntent(pendingIntent)
            .build()
        mediaPlayer!!.start()
        val pattern = longArrayOf(0, 100, 1000)
        vibrator!!.vibrate(pattern, 0)
        startForeground(1, notification)
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer!!.stop()
        vibrator!!.cancel()
    }

    @Nullable
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}