package com.primeiro.nutripet

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.primeiro.nutripet.CreateAlarm.CreateAlarmViewModel
import com.primeiro.nutripet.CreateAlarm.TimePickerUtil
import com.primeiro.nutripet.Data.Alarm
import com.primeiro.nutripet.Data.AlarmRepository
import kotlinx.android.synthetic.main.activity_create_alarm.*
import java.util.*

class CreateAlarmActivity : AppCompatActivity() {

    lateinit var timePicker: TimePicker
    lateinit var title: TextView
    lateinit var scheduleAlarm: Button
    lateinit var recurring: CheckBox
    lateinit var mon:CheckBox
    lateinit var tue:CheckBox
    lateinit var wed:CheckBox
    lateinit var thu:CheckBox
    lateinit var fri:CheckBox
    lateinit var sat:CheckBox
    lateinit var sun:CheckBox
    lateinit var recurringOptions: LinearLayout
    lateinit var createAlarmViewModel: CreateAlarmViewModel

    lateinit var managerDietData: DietData
    var food: String? = null
    var petName: String? = null
    var lunchName: String? = null
    var dinnerName: String? = null
    var petID: String? = null
    var lunchTime: String? = null
    var dinnerTime: String? = null
    var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_alarm)

        position = intent.getIntExtra("position", 0)
        petID = intent.getStringExtra("PET_ID")
        petName = intent.getStringExtra("PET_NAME")
        food = intent.getStringExtra("FOOD")
        lunchName = intent.getStringExtra("LUNCH_NAME")
        dinnerName = intent.getStringExtra("DINNER_NAME")

        lunchTime = intent.getStringExtra("LUNCH_TIME")
        dinnerTime = intent.getStringExtra("DINNER_TIME")


        managerDietData = DietData(applicationContext, petID)

        timePicker = create_alarm_time_picker
        timePicker.setIs24HourView(true)
        title = create_alarm_title
        if (food == "LUNCH") {
            title.setText(String.format("Pet: %s \nRefeição: %s", petName, lunchName))
            managerDietData.SaveLunchAlarmId(0)
        } else if (food == "DINNER") {
            title.setText(String.format("Pet: %s \nRefeição: %s", petName, dinnerName))
            managerDietData.SaveDinnerAlarmId(0)
        }
        scheduleAlarm = button_schedule_alarm
        recurring = create_alarm_recurring
        mon = create_alarm_check_mon
        tue = create_alarm_check_tue
        wed = create_alarm_check_wed
        thu = create_alarm_check_thu
        fri = create_alarm_check_fri
        sat = create_alarm_check_sat
        sun = create_alarm_check_sun
        recurringOptions = create_alarm_recurring_options
        createAlarmViewModel = ViewModelProviders.of(this)[CreateAlarmViewModel::class.java]


        recurring.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                if (isChecked) {
                    recurringOptions.setVisibility(View.VISIBLE)
                } else {
                    recurringOptions.setVisibility(View.GONE)
                }
            }
        })
        scheduleAlarm.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                if(recurring.isChecked) {
                    if (mon.isChecked || tue.isChecked || wed.isChecked || thu.isChecked
                            || fri.isChecked || sat.isChecked || sun.isChecked) {
                        createAlarm()
                        startDietRegistrationActivity()
                    } else {
                        Toast.makeText(baseContext, "Selecione os dias do alarme", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    startDietRegistrationActivity()
                }
            }
        })
    }

    private fun startDietRegistrationActivity() {
        var intent = Intent(baseContext, DietRegistrationScreen::class.java)
        intent.putExtra("position", position)
        intent.putExtra("PET_ID", petID)
        intent.putExtra("PET_NAME", petName)
        intent.putExtra("LUNCH_TIME", lunchTime)
        intent.putExtra("LUNCH_NAME", lunchName)
        intent.putExtra("DINNER_TIME", dinnerTime)
        intent.putExtra("DINNER_NAME", dinnerName)
        intent.putExtra(food + "_TIME", String.format("%02d:%02d",
                TimePickerUtil.getTimePickerHour(timePicker),
                TimePickerUtil.getTimePickerMinute(timePicker)))
        startActivity(intent)
        finish()
    }

    private fun createAlarm() {
        val alarmId = Random().nextInt(Integer.MAX_VALUE)
        if (food == "LUNCH") {
            managerDietData.SaveLunchAlarmId(alarmId)
        } else if (food == "DINNER") {
            managerDietData.SaveDinnerAlarmId(alarmId)
        }
        val alarm = Alarm(
            alarmId,
            TimePickerUtil.getTimePickerHour(timePicker),
            TimePickerUtil.getTimePickerMinute(timePicker),
            title!!.getText().toString(),
            System.currentTimeMillis(),
            false,
            recurring.isChecked(),
            false,
            mon.isChecked(),
            tue.isChecked(),
            wed.isChecked(),
            thu.isChecked(),
            fri.isChecked(),
            sat.isChecked(),
            sun.isChecked()
        )

        createAlarmViewModel.insert(alarm)
    }

    override fun onBackPressed() {
        startDietRegistrationActivity()
    }
}