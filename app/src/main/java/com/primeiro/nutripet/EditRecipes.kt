package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_edit_recipes.*


class EditRecipes : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_recipes)

        val manage_edit_recipe_data = RecipeData(applicationContext)
        val recipeID = intent.getIntExtra("RECIPE_ID", 0)

        var init_recipename = manage_edit_recipe_data.RecoverNameRecipe().toString().split(",").toMutableList()
        var init_ingredients = manage_edit_recipe_data.RecoverIngredients().toString().split(",").toMutableList()
        var init_preparation_mode = manage_edit_recipe_data.RecoverPreparationMode().toString().split(",").toMutableList()

        edit_recipe_name.setText(init_recipename[recipeID + 1])
        edit_ingredients.setText(init_ingredients[recipeID + 1])
        edit_preparation_mode.setText(init_preparation_mode[recipeID + 1])


        bt_edit_recipe.setOnClickListener{
            var recipename11 = manage_edit_recipe_data.RecoverNameRecipe().toString().split(",").toMutableList()
            var ingredients11 = manage_edit_recipe_data.RecoverIngredients().toString().split(",").toMutableList()
            var preparation_mode11 = manage_edit_recipe_data.RecoverPreparationMode().toString().split(",").toMutableList()

            if (edit_recipe_name.text.toString() == "" || edit_recipe_name.text.toString().contains(",") || edit_recipe_name.text.toString().trim() == "") {
                Toast.makeText(this, "Digite o nome da receita e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (edit_ingredients.text.toString() == "" || edit_ingredients.text.toString().contains(",") || edit_ingredients.text.toString().trim() == "") {
                Toast.makeText(this, "Digite ao menos um ingrediente e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (edit_preparation_mode.text.toString() == "" || edit_preparation_mode.text.toString().contains(",") || edit_preparation_mode.text.toString().trim() == "") {
                Toast.makeText(this, "Digite um modo de preparo e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else {
                recipename11[recipeID + 1] = edit_recipe_name.text.toString()
                ingredients11[recipeID + 1] = edit_ingredients.text.toString()
                preparation_mode11[recipeID + 1] = edit_preparation_mode.text.toString()

                var recipename = recipename11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
                var ingredients = ingredients11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
                var preparation_mode = preparation_mode11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")

                manage_edit_recipe_data.SaveNameRecipe("$recipename")
                manage_edit_recipe_data.SaveIngredients("$ingredients")
                manage_edit_recipe_data.SavePreparationMode("$preparation_mode")

                Toast.makeText(this, "Receita atualizada com sucesso!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewRecipes::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onBackPressed() {
        val recipeID = intent.getIntExtra("RECIPE_ID", 0)
        var intent = Intent(this, ViewRecipesInfo::class.java)
        intent.putExtra("position", recipeID)
        startActivity(intent)
        finish()
    }
}