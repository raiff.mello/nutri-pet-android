package com.primeiro.nutripet

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.primeiro.nutripet.CreateAlarm.CreateAlarmViewModel
import com.primeiro.nutripet.Data.Alarm
import com.primeiro.nutripet.Data.AlarmRepository
import kotlinx.android.synthetic.main.activity_diet_registration_screen.*

class DietRegistrationScreen : AppCompatActivity() {
    var petID: String? = null
    var lunchTime: String? = null
    var dinnerTime: String? = null
    var petName: String? = null
    var alarmLunchID:Int? = null
    var alarmDinnerID:Int? = null
    var alarmLunch:Alarm? = null
    var alarmDinner:Alarm? = null
    var position: Int = 0

    lateinit var managerDietData: DietData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diet_registration_screen)

        position = intent.getIntExtra("position", 0)
        petID = intent.getStringExtra("PET_ID")
        petName = intent.getStringExtra("PET_NAME")
        lunchTime = intent.getStringExtra("LUNCH_TIME")
        dinnerTime = intent.getStringExtra("DINNER_TIME")

        edit_lunch.setText(intent.getStringExtra("LUNCH_NAME"))
        edit_dinner.setText(intent.getStringExtra("DINNER_NAME"))

        managerDietData = DietData(applicationContext, petID)

        alarmLunchID = managerDietData.RetrieveLunchAlarmId()
        alarmDinnerID = managerDietData.RetrieveDinnerAlarmId()

        getAlarms()

        listenerAlarmTimePicker(edit_time_lunch, image_button_lunch_alarm, alarmLunchID, lunchTime, "LUNCH")
        listenerAlarmTimePicker(edit_time_dinner, image_button_dinner_alarm, alarmDinnerID, dinnerTime, "DINNER")


        button_register_diet.setOnClickListener {
            var lunch = edit_lunch.text.toString()
            var timeLunch = edit_time_lunch.text.toString()
            var dinner = edit_dinner.text.toString()
            var timeDinner = edit_time_dinner.text.toString()
            if(lunch.isNullOrEmpty()) {
                Toast.makeText(this, "Digite o almoço", Toast.LENGTH_SHORT).show()
            } else if(timeLunch.isNullOrEmpty()) {
                Toast.makeText(this, "Selecione o horário do almoço", Toast.LENGTH_SHORT).show()
            } else if(dinner.isNullOrEmpty()){
                Toast.makeText(this, "Digite a janta", Toast.LENGTH_SHORT).show()
            } else if(timeDinner.isNullOrEmpty()) {
                Toast.makeText(this, "Selecione o horário da janta", Toast.LENGTH_SHORT).show()
            } else {
                managerDietData.SaveLunchName(lunch)
                managerDietData.SaveLunchTime(timeLunch)
                managerDietData.SaveDinnerName(dinner)
                managerDietData.SaveDinnerTime(timeDinner)

                scheduleAlarms()

                Toast.makeText(this, "Dieta cadastrada!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewPetsInfo::class.java)
                intent.putExtra("position", position)
                startActivity(intent)
                finish()
            }
        }

    }

    private fun getAlarms() {
        val alarmRepository = AlarmRepository(application)
        alarmRepository.getAlarmLiveData(alarmLunchID)
                .observe(this, Observer<List<Alarm>> { alarms ->
                    for (alarm in alarms) {
                        if (alarm.isRecurring) {
                            alarmLunch = alarm
                        }
                    }
                })
        alarmRepository.getAlarmLiveData(alarmDinnerID)
                .observe(this, Observer<List<Alarm>> { alarms ->
                    for (alarm in alarms) {
                        if (alarm.isRecurring) {
                            alarmDinner = alarm
                        }
                    }
                })
    }

    fun scheduleAlarms() {
        val createAlarmViewModel = ViewModelProviders.of(this)[CreateAlarmViewModel::class.java]

        if(alarmLunch != null) {
            alarmLunch!!.schedule(applicationContext)
            createAlarmViewModel.update(alarmLunch)
        }
        if(alarmDinner != null) {
            alarmDinner!!.schedule(applicationContext)
            createAlarmViewModel.update(alarmDinner)
        }
    }


    fun listenerAlarmTimePicker(editText: EditText, imageButton: ImageButton, alarmID: Int?, time: String?, food: String) {
        imageButton.imageAlpha = 75
        if (time != null) {
            editText.setText(time)
            if(alarmID != 0) {
                imageButton.imageAlpha = 255
            }
        }
        editText.setOnClickListener {
            val intent = Intent(this, CreateAlarmActivity::class.java)
            intent.putExtra("position", position)
            intent.putExtra("PET_ID", petID)
            intent.putExtra("PET_NAME", petName)
            intent.putExtra("FOOD", food)
            intent.putExtra("LUNCH_TIME", lunchTime)
            intent.putExtra("DINNER_TIME", dinnerTime)
            intent.putExtra("LUNCH_NAME", edit_lunch.text.toString())
            intent.putExtra("DINNER_NAME", edit_dinner.text.toString())
            startActivity(intent)
            finish()
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, ViewPetsInfo::class.java)
        intent.putExtra("position", position)
        startActivity(intent)
        finish()
    }
}