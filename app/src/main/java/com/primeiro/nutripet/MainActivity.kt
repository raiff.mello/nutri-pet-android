package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

/*
Main class responsible for launching the app as well as forwarding according to the registered data
to the corresponding screen.
 */

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.hide()                               // Hide header from login screen.

        var screenLogin = OwnerData(applicationContext)         // Calling OwnerData class to var;
        var buttonLogin = button_init_main_screen                        // Login screen button.

        if(screenLogin.InitStatus() == "register"){
            // If there is no registered owner, the button opens the owner registration screen.
            buttonLogin.setOnClickListener {
                var intent = Intent(this, OwnerRegistrationScreen::class.java)
                startActivity(intent)
                finish()
            }
        }else{
            // If you have a registered owner, the button opens the home menu screen.
            buttonLogin.setText("Menu")
            buttonLogin.setOnClickListener {
                var intent = Intent(this, InitMenuScreen::class.java)
                startActivity(intent)
                finish()
            }
        }

    }
    override fun onBackPressed() {
        finish()
    }
}
