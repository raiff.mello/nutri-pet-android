package com.primeiro.nutripet

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.primeiro.nutripet.R.*
import com.primeiro.nutripet.R.color.*
import kotlinx.android.synthetic.main.activity_register_specific_weight.*
import java.util.*

class RegisterSpecificWeight : AppCompatActivity() {
    var petID: String? = null
    lateinit var managerWeightData: WeightData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_register_specific_weight)

        petID = intent.getStringExtra("PET_ID")
        managerWeightData = WeightData(applicationContext, petID)
        var specificWeight = managerWeightData.RetrieveSpecificWeight().toString().split(",").toMutableList()
        var day = managerWeightData.RetrieveDay().toString().split(",").toMutableList()
        edit_day.setOnClickListener {
            val now = Calendar.getInstance()
            val datePicker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{view, year, month, dayOfMonth ->
                edit_day.setText("$dayOfMonth/${month+1}")
            },
                now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH))
            datePicker.show()
        }

        bt_register_specific_weight.setOnClickListener {
            if(edit_specific_weight.text.toString().isNullOrEmpty() || edit_specific_weight.text.toString().contains(",")) {
                Toast.makeText(this, "Digite o peso e utilize ponto ao invés de vírgula", Toast.LENGTH_SHORT).show()
            } else if(edit_day.text.toString().isNullOrEmpty() || edit_day.text.toString().contains(",")) {
                Toast.makeText(this, "Selecione a data específica", Toast.LENGTH_SHORT).show()
            }else if (specificWeight.count() == 11) {
                var weight1 = managerWeightData.RetrieveSpecificWeight().toString().split(",").toMutableList()
                var day1 = managerWeightData.RetrieveDay().toString().split(",").toMutableList()
                for (i in 1..9) {
                    weight1[i] = specificWeight[i+1].removePrefix(" ").removeSuffix(" ")
                    day1[i] = day[i+1].removePrefix(" ").removeSuffix(" ")
                }
                weight1[10] = edit_specific_weight.text.toString().removePrefix(" ").removeSuffix(" ")
                day1[10] = edit_day.text.toString().removePrefix(" ").removeSuffix(" ")

                var weight11 = weight1.toString().removePrefix("[").removeSuffix("]")
                var day11 = day1.toString().removePrefix("[").removeSuffix("]")

                managerWeightData.SaveSpecificWeight("$weight11")
                managerWeightData.SaveDay("$day11")
                Toast.makeText(this, "Peso específico cadastrado!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewPetsInfo::class.java)
                intent.putExtra("position", petID)
                startActivity(intent)
                finish()
            }else{
                var specificWeight = managerWeightData.RetrieveSpecificWeight()
                specificWeight += "," + edit_specific_weight.text.toString()

                var day = managerWeightData.RetrieveDay()
                day += "," + edit_day.text.toString()

                managerWeightData.SaveSpecificWeight(specificWeight)
                managerWeightData.SaveDay(day)

                Toast.makeText(this, "Peso específico cadastrado!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewPetsInfo::class.java)
                intent.putExtra("position", petID)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, ViewPetsInfo::class.java)
        intent.putExtra("position", petID)
        startActivity(intent)
        finish()
    }
}