package com.primeiro.nutripet

import android.content.Context
import android.content.SharedPreferences
import java.io.File

class WeightData(private val context: Context, private val petID: String?) {

    private val WEIGHT_FILE = "data.weight"

    private val WEIGHT = "WEIGHT"
    private val DAY = "DAY"

    private val preferences: SharedPreferences
    private val editor: SharedPreferences.Editor


    fun SaveSpecificWeight(data: String?){
        editor.putString(WEIGHT, data)
        editor.commit()
    }
    fun SaveDay(data: String?){
        editor.putString(DAY, data)
        editor.commit()
    }

    fun RetrieveSpecificWeight(): String? {
        return preferences.getString(WEIGHT, "")
    }

    fun RetrieveDay(): String? {
        return preferences.getString(DAY, "")
    }

    fun DeleteWeightData() {
        preferences.edit().clear().commit()
        File("/data/data/com.primeiro.nutripet/shared_prefs/data.weight" + petID + ".xml").delete()
    }

    init {
        // Function that initializes sharedpreferences
        preferences = context.getSharedPreferences(WEIGHT_FILE + petID, 0)
        editor = preferences.edit()
    }
}
