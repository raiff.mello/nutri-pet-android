package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import kotlinx.android.synthetic.main.activity_edit_owner.*

class EditOwner : AppCompatActivity() {
    var checkNumber = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_owner)

        val manage_owner_data = OwnerData(applicationContext)                                       // Variable that receives OwnerData;
        val text_field_data = arrayOf(edit_name_owner, edit_email_owner, edit_phone_owner)    // Owner information collected from the activity;

        edit_phone_owner.doAfterTextChanged {
            checkNumber = edit_phone_owner.isDone
        }

        edit_name_owner.setText(manage_owner_data.RetrieveOwnerName().toString())
        edit_email_owner.setText(manage_owner_data.RetrieveOwnerEmail().toString())
        edit_phone_owner.setText(manage_owner_data.RetrieveOwnerPhone().toString())
        checkNumber = true

        button_edit_owner_info.setOnClickListener {
            val owner_name = text_field_data[0].text.toString()         // Owner name collected from the activity;
            val owner_mail = text_field_data[1].text.toString()         // Owner email collected from the activity;
            val owner_phone = text_field_data[2].text.toString()        // Owner phone collected from the activity;

            if (owner_name == "") {             // Checking empty name field;
                Toast.makeText(this, "Digite o seu nome", Toast.LENGTH_SHORT).show()
            } else if (owner_mail == "") {      // Checking empty email field;
                Toast.makeText(this, "Digite um email", Toast.LENGTH_SHORT).show()
            } else if (!verifyEmail(owner_mail)) {      // Checking empty email field;
                Toast.makeText(this, "Digite um email válido", Toast.LENGTH_SHORT).show()
            } else if (owner_phone == "") {      // Checking empty email field;
                Toast.makeText(this, "Digite um telefone", Toast.LENGTH_SHORT).show()
            } else if (!checkNumber) {     // Checking empty phone field;
                Toast.makeText(this, "Digite um telefone válido", Toast.LENGTH_SHORT).show()
            } else {
                manage_owner_data.SaveOwnerName(owner_name)                // edit owner name in local file;
                manage_owner_data.SaveEmailOwner(owner_mail)               // edit owner email in local file;
                manage_owner_data.SaveOwnerPhone(owner_phone)              // edit owner phone in local file;
                Toast.makeText(this, "As informações do dono foram alteradas!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewOwner::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onBackPressed() {
        var intent = Intent(this, ViewOwner::class.java)
        startActivity(intent)
        finish()
    }
    fun verifyEmail(email: String):Boolean{
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        }
        return false;
    }
}