package com.primeiro.nutripet

import android.app.TimePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_diet_registration_screen.*
import kotlinx.android.synthetic.main.activity_edit_diet.*
import kotlinx.android.synthetic.main.activity_edit_substitute_food.*
import java.text.SimpleDateFormat
import java.util.*

class EditDiet : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_diet)

        val petID = intent.getStringExtra("PET_ID")
        val manage_diet_data = DietData(applicationContext, petID)

        dialogTimePicker(text_edit_time_lunch)
        dialogTimePicker(text_edit_time_dinner)

        text_edit_lunch.setText(manage_diet_data.RetrieveLunchName().toString())
        text_edit_time_lunch.setText(manage_diet_data.RetrieveLunchTime().toString())
        text_edit_dinner.setText(manage_diet_data.RetrieveDinnerName().toString())
        text_edit_time_dinner.setText(manage_diet_data.RetrieveDinnerTime().toString())

        button_edit_diet.setOnClickListener {
            val text_lunch_name = text_edit_lunch.text.toString()                  // Food Lunch collected from the activity;
            val text_lunch_time = text_edit_time_lunch.text.toString()             // Time Lunch collected from the activity;
            val text_dinner_name = text_edit_dinner.text.toString()                // Food Dinner collected from the activity;
            val text_dinner_time = text_edit_time_dinner.text.toString()           // Time Dinner collected from the activity;

            if(text_lunch_name.isNullOrEmpty()) {
                Toast.makeText(this, "Digite o almoço", Toast.LENGTH_SHORT).show()
            } else if(text_lunch_time.isNullOrEmpty()) {
                Toast.makeText(this, "Selecione o horário do almoço", Toast.LENGTH_SHORT).show()
            } else if(text_dinner_name.isNullOrEmpty()){
                Toast.makeText(this, "Digite a janta", Toast.LENGTH_SHORT).show()
            } else if(text_dinner_time.isNullOrEmpty()) {
                Toast.makeText(this, "Selecione o horário da janta", Toast.LENGTH_SHORT).show()
            } else {
                manage_diet_data.SaveLunchName(text_lunch_name)                        // Saving edit lunch in local file;
                manage_diet_data.SaveLunchTime(text_lunch_time)                        // Saving edit time lunch in local file;
                manage_diet_data.SaveDinnerName(text_dinner_name)                      // Saving edit dinner in local file;
                manage_diet_data.SaveDinnerTime(text_dinner_time)                      // Saving edit time dinner in local file;

                Toast.makeText(this, "A Dieta foi atualizada!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewPets::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
    fun dialogTimePicker(editText: EditText) {
        editText.setOnClickListener {

            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                editText.setText(SimpleDateFormat("HH:mm").format(cal.time))
            }
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
        }
    }
    override fun onBackPressed() {
        val petID = intent.getStringExtra("PET_ID")
        var intent = Intent(this, ViewDiet::class.java)
        intent.putExtra("PET_ID", petID)
        startActivity(intent)
        finish()
    }
}