package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_pet_registration_screen.*

class PetRegistrationScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pet_registration_screen)
        bt_register_pet.setText("Cadastrar Pet")

        val dataPet = DataPet(applicationContext)


        val savePet = arrayOf(edit_pet_name, edit_animal, edit_race, edit_weight, edit_age)
        var initialStatus = "not registered"


        bt_register_pet.setOnClickListener {
            if (savePet[0].text.toString() == "" || savePet[0].text.toString().contains(",")) {
                Toast.makeText(this, "Digite o nome do pet e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (savePet[1].text.toString() == "" || savePet[1].text.toString().contains(",")) {
                Toast.makeText(this, "Digite um tipo de animal e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (savePet[2].text.toString() == "" || savePet[2].text.toString().contains(",")) {
                Toast.makeText(this, "Digite uma raça e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (savePet[3].text.toString() == "") {
                Toast.makeText(this, "Digite um peso", Toast.LENGTH_SHORT).show()
            } else if (savePet[4].text.toString() == "") {
                Toast.makeText(this, "Digite uma idade", Toast.LENGTH_SHORT).show()
            } else {
                var petsname = dataPet.RecoverNamePet()
                petsname += "," + savePet[0].text.toString()

                var animal = dataPet.RecoverTypeAnimal()
                animal += "," + savePet[1].text.toString()

                var race = dataPet.RecoverRace()
                race += "," + savePet[2].text.toString()

                var weight = dataPet.RecoverWeight()
                weight += "," + savePet[3].text.toString()

                var age = dataPet.RecoverAge()
                age += "," + savePet[4].text.toString()

                var id = dataPet.RecoverIdPet()
                id += "," + dataPet.RecoverIdClass().toString()

                dataPet.SaveNamePet(petsname)
                dataPet.SaveTypeAnimal(animal)
                dataPet.SaveRace(race)
                dataPet.SaveWeight(weight)
                dataPet.SaveAge(age)
                dataPet.SaveIdPet(id)
                dataPet.IncrementIdClass()
                Toast.makeText(this, "O Pet foi cadastrado!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewPets::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onBackPressed() {
        var intent = Intent(this, ViewPets::class.java)
        startActivity(intent)
        finish()
    }
}