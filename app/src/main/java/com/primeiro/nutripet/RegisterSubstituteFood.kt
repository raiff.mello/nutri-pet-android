package com.primeiro.nutripet

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_edit_pet_info.*
import kotlinx.android.synthetic.main.activity_owner_registration_screen.*
import kotlinx.android.synthetic.main.activity_register_substitute_food.*

class RegisterSubstituteFood : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_substitute_food)

        val petID = intent.getStringExtra("PET_ID")

        val manage_substitute_food_data = SubstituteFoodData(applicationContext, petID)    // Variable that receives SubstituteFoodData;

        val register_button_substitute_food = button_subscribe_substitute_food      // Button to register the collected data;


        register_button_substitute_food.setOnClickListener {
            val substitute_lunch = register_substitute_lunch.text.toString()    // Substitute Food Lunch collected from the activity;
            val substitute_dinner = register_substitute_dinner.text.toString()  // Substitute Food Dinner collected from the activity;

            if (substitute_lunch == "") {
                Toast.makeText(this, "Digite um alimento substituto para o almoço", Toast.LENGTH_SHORT).show()
            }else if (substitute_dinner == "") {
                Toast.makeText(this, "Digite um alimento substituto para o jantar", Toast.LENGTH_SHORT).show()
            }else {
                manage_substitute_food_data.ChangeStatusSubstituteFood("menu")    // Changing control state of the login screen;
                manage_substitute_food_data.SaveSubstituteFoodLunch(substitute_lunch)               // Saving substitute lunch in local file;
                manage_substitute_food_data.SaveSubstituteFoodDinner(substitute_dinner)             // Saving substitute dinner in local file;
                Toast.makeText(this, "Alimentos substitutos cadastrados!", Toast.LENGTH_SHORT).show()

                var intent = Intent(this, ViewDiet::class.java)
                intent.putExtra("PET_ID", petID)
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onBackPressed() {
        val petID = intent.getStringExtra("PET_ID")
        var intent = Intent(this, ViewDiet::class.java)
        intent.putExtra("PET_ID", petID)
        startActivity(intent)
        finish()
    }
}