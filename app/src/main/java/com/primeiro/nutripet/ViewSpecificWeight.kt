package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.activity_view_specific_weight.*

class ViewSpecificWeight : AppCompatActivity() {
    var petID: String? = null
    lateinit var managerWeightData: WeightData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_specific_weight)
        petID = intent.getStringExtra("PET_ID")
        managerWeightData = WeightData(applicationContext, petID)

        if (managerWeightData.RetrieveSpecificWeight().toString().split(",").toMutableList().size == 1){
            lineChart.setNoDataText("")
            dialogEmpty()
        }else{
            setLineChartData(managerWeightData)
        }
        bt_back_menu_pets.setOnClickListener {
            var intent = Intent(this, ViewPetsInfo::class.java)
            intent.putExtra("position", petID)
            startActivity(intent)
            finish()
        }
    }

    fun setLineChartData(managerWeightData: WeightData){
        var weight = managerWeightData.RetrieveSpecificWeight().toString().split(",").toMutableList()
        var day = managerWeightData.RetrieveDay().toString().split(",").toMutableList()

        val xvalue = ArrayList<String>()
        for (i in 1..day.size-1) {
            xvalue.add(day[i])
        }


        val lineentry = ArrayList<Entry>()
        for (i in 1..weight.size-1){
            lineentry.add(Entry(weight[i].toFloat(), i-1))
        }


        val lineDataSet = LineDataSet(lineentry, "Peso")
        lineDataSet.color = resources.getColor(R.color.green90)


        lineDataSet.setCircleColor(resources.getColor(R.color.green90))
        lineDataSet.setDrawFilled(true)
        lineDataSet.fillColor = resources.getColor(R.color.green90)
        lineDataSet.fillAlpha = 60
        lineDataSet.lineWidth = 3f

        val data = LineData(xvalue, lineDataSet)
        lineChart.data = data
        lineChart.setBackgroundColor(resources.getColor(R.color.white))
        lineChart.animateXY(2000, 2000)

        //cria o zoom, e o toque no gráfico
        lineChart.setTouchEnabled(true)
        lineChart.setPinchZoom(true)

        //valores no lado esquerdo referente ao eixo Y
        lineChart.axisRight.isEnabled = false

        //Descrição
        lineChart.setDescription("Days")

        lineChart.setNoDataText("")

    }
    override fun onBackPressed() {
        var intent = Intent(this, ViewPetsInfo::class.java)
        intent.putExtra("position", petID)
        startActivity(intent)
        finish()
    }

    fun dialogEmpty() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Primeiro você deve cadastrar algum peso específico")
            .setCancelable(false)
            .setNegativeButton("Ok") { dialog, id ->
                var intent = Intent(this, ViewPetsInfo::class.java)
                intent.putExtra("position", petID)
                startActivity(intent)
                finish()
                dialog.dismiss()
            }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
