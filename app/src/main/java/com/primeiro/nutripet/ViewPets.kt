package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.primeiro.nutripet.Adapters.OnItemClickListenerPet
import com.primeiro.nutripet.Adapters.ViewPetDataAdapter
import com.primeiro.nutripet.Adapters.addOnItemClickListenerPet
import kotlinx.android.synthetic.main.activity_view_pets.*


class ViewPets : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_pets)

        val pets = DataPet(applicationContext)
        var intent = Intent(this, ViewPetsInfo::class.java)
        recycler_pets.removeAllViewsInLayout()
        var retorno:MutableList<DataPet.ViewPetData> = mutableListOf()

        var name1 = pets.RecoverNamePet().toString().split(",").toMutableList()
        var animal1 = pets.RecoverTypeAnimal().toString().split(",").toMutableList()
        var race1 = pets.RecoverRace().toString().split(",").toMutableList()
        var weight1 = pets.RecoverWeight().toString().split(",").toMutableList()
        var age1 = pets.RecoverAge().toString().split(",").toMutableList()

        if (name1.size == 1){
            empty.setText("Parece que não tem nenhum pet aqui!")
        }else{
            empty.setText("")
            for (i in 1..name1.count()-1){
                var view = pets.viewPetData {
                    if (name1[i].length>=11){
                        name = "Nome: " + name1.get(i).substring(0..11) + "..."
                    }else {
                        name = "Nome: " + name1.get(i)
                    }
                    if (animal1[i].length>=11){
                        animal = "Animal: " + animal1.get(i).substring(0..11) + "..."
                    }else{
                        animal = "Animal: " + animal1.get(i)
                    }
                    if (race1[i].length>=11){
                        race = "Raça: " + race1.get(i).substring(0..11) + "..."
                    }else{
                        race = "Raça: " + race1.get(i)
                    }
                    if (weight1[i].endsWith(".") && weight1[i].startsWith(".")){
                        weight = "Peso: 0" + weight1.get(i) + "0 Kg"
                    }else if(weight1[i].startsWith(".")){
                        weight = "Peso: 0" + weight1.get(i) + " Kg"
                    }else if(weight1[i].endsWith(".")) {
                        weight = "Peso: " + weight1.get(i) + "0 Kg"
                    }else{
                        weight = "Peso: " + weight1.get(i) + " Kg"
                    }
                    age = "Idade: " + age1.get(i)

                }
                retorno = pets.addViewPetData(view, retorno)
            }

            recycler_pets.adapter = ViewPetDataAdapter(retorno)
            recycler_pets.layoutManager = LinearLayoutManager(this)

            recycler_pets.addOnItemClickListenerPet(object: OnItemClickListenerPet {
                override fun onItemClickedPet(position: Int, view: View) {
                    for (ii in 0..retorno.size)
                        if (position == ii){
                            intent.putExtra("position", position)
                            startActivity(intent)
                            finish()
                        }
                }
            })
        }

        bt_back_menu.setOnClickListener {
            var intent = Intent(this, InitMenuScreen::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflate = menuInflater
        inflate.inflate(R.menu.menu_list_pets_recipes, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.add_pet_recipes){
            var intent = Intent(this, PetRegistrationScreen::class.java)
            startActivity(intent)
            finish()
        }
        return true
    }

    override fun onBackPressed() {
        var intent = Intent(this, InitMenuScreen::class.java)
        startActivity(intent)
        finish()
    }
}