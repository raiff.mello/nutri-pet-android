package com.primeiro.nutripet

import android.content.Context
import android.content.SharedPreferences

/*
This class is responsible for administering the application user registration information locally.
Internal storage with SharedPreferences was used here. Of the functions present, it is possible to
save data(in type String) necessary for registration, it is possible to specifically retrieve data or all data
 through a list and there is also a function that excludes the registration data.

 */

class OwnerData(private val context: Context) {

    private val OWNERFILE = "data.dono"         // Local archive
    private val OWNERNAME = "OWNERNAME"         // Key to owner's name
    private val OWNERMAIL = "OWNERMAIL"         // Key to owner's email
    private val OWNERPHONE = "OWNERPHONE"       // Key to owner's phone
    private val SCREENLOGIN = "register"        // Control variable for login screen

    private val preferences: SharedPreferences
    private val editor: SharedPreferences.Editor

    fun SaveOwnerName(dados: String?){
        // Function to save the owner's name in the local file.
        editor.putString(OWNERNAME, dados)
        editor.commit()
    }
    fun SaveEmailOwner(dados: String?){
        // Function to save the owner's email in the local file.
        editor.putString(OWNERMAIL, dados)
        editor.commit()
    }
    fun SaveOwnerPhone(dados: String?){
        // Function to save the owner's phone in the local file.
        editor.putString(OWNERPHONE, dados)
        editor.commit()
    }

    fun RetrieveOwnerDataList(): Array<String?> {
        //Function that returns the owner data saved in the local file in a list.
        val dad = arrayOf(preferences.getString(OWNERNAME, ""),
                            preferences.getString(OWNERMAIL, ""),
                            preferences.getString(OWNERPHONE, ""))
        return dad
    }

    fun RetrieveOwnerName(): String? {
        // Function that returns the name of the owner saved in the local file.
        return preferences.getString(OWNERNAME, "")
    }

    fun RetrieveOwnerEmail(): String? {
        // Function that returns the email of the owner saved in the local file.
        return preferences.getString(OWNERMAIL, "")
    }
    fun RetrieveOwnerPhone(): String? {
        // Function that returns the owner's phone saved in the local file.
        return preferences.getString(OWNERPHONE, "")
    }

    fun DeleteOwnerData(): String? {
        // Function responsible for deleting the owner data saved in the local file.
        SaveOwnerName("")
        SaveEmailOwner("")
        SaveOwnerPhone("")
        ChangeStatus("register")
        return "Owner Registration Successfully deleted!"
    }

    fun ChangeStatus(screen_init: String?) {
        // Function responsible for modifying the login screen control variable.
        editor.putString(SCREENLOGIN, screen_init)
        editor.commit()
    }
    fun InitStatus(): String?{
        // Function responsible for returning the control variable from the login screen.
        return preferences.getString(SCREENLOGIN, "register")
    }
    init {      // Function that initializes sharedpreferences
        preferences = context.getSharedPreferences(OWNERFILE, 0)
        editor = preferences.edit()
    }

    }
