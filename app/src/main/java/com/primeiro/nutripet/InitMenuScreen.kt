package com.primeiro.nutripet

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_init_menu_screen.*


/*
Home class that acts as a navigation menu to view the information present saved in the application.
 */

class InitMenuScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_init_menu_screen)

        supportActionBar!!.hide()

        button_owner_init_screen.setOnClickListener {
            var intent = Intent(this, ViewOwner::class.java)
            startActivity(intent)
            finish()
        }

        button_pets_init_screen.setOnClickListener {
            var intent = Intent(this, ViewPets::class.java)
            startActivity(intent)
            finish()
        }

        button_recipes_init_screen.setOnClickListener {
            var intent = Intent(this, ViewRecipes::class.java)
            startActivity(intent)
            finish()
        }

        button_clinics_init_screen.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Clínicas veterinárias")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }

    }
    override fun onBackPressed() {
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
