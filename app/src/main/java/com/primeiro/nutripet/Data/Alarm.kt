package com.primeiro.nutripet.Data

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.FRIDAY
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.MONDAY
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.RECURRING
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.SATURDAY
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.SUNDAY
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.THURSDAY
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.TITLE
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.TUESDAY
import com.primeiro.nutripet.Alarm.AlarmBroadcastReceiver.Companion.WEDNESDAY
import com.primeiro.nutripet.CreateAlarm.CreateAlarmViewModel
import com.primeiro.nutripet.CreateAlarm.DayUtil.toDay
import java.util.*


@Entity(tableName = "alarm_table")
class Alarm(
        @field:PrimaryKey
        var alarmId: Int,
        val hour: Int,
        val minute: Int,
        val title: String,
        var created: Long,
        var isStarted: Boolean,
        val isRecurring: Boolean,
        val isSnooze: Boolean,
        val isMonday: Boolean,
        val isTuesday: Boolean,
        val isWednesday: Boolean,
        val isThursday: Boolean,
        val isFriday: Boolean,
        val isSaturday: Boolean,
        val isSunday: Boolean
) {

    fun schedule(context: Context) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, AlarmBroadcastReceiver::class.java)
        intent.putExtra(RECURRING, isRecurring)
        intent.putExtra(MONDAY, isMonday)
        intent.putExtra(TUESDAY, isTuesday)
        intent.putExtra(WEDNESDAY, isWednesday)
        intent.putExtra(THURSDAY, isThursday)
        intent.putExtra(FRIDAY, isFriday)
        intent.putExtra(SATURDAY, isSaturday)
        intent.putExtra(SUNDAY, isSunday)
        intent.putExtra(TITLE, title)
        val alarmPendingIntent = PendingIntent.getBroadcast(
                context,
                alarmId,
                intent,
                0
        )
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[Calendar.HOUR_OF_DAY] = hour
        calendar[Calendar.MINUTE] = minute
        calendar[Calendar.SECOND] = 0
        calendar[Calendar.MILLISECOND] = 0

        // if alarm time has already passed, increment day by 1
        if (calendar.timeInMillis <= System.currentTimeMillis()) {
            calendar[Calendar.DAY_OF_MONTH] = calendar[Calendar.DAY_OF_MONTH] + 1
        }
        if (isRecurring) {
            val toastText = String.format(
                    "Alarme recorrente %s agendado para %s às %02d:%02d",
                    title,
                    recurringDaysText,
                    hour,
                    minute,
                    alarmId
            )
            Toast.makeText(context, toastText, Toast.LENGTH_LONG).show()
            val RUN_DAILY = (24 * 60 * 60 * 1000).toLong()
            alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis,
                    RUN_DAILY,
                    alarmPendingIntent
            )
        } else if (isSnooze) {
            var toastText: String? = null
            try {
                toastText = String.format("Alarme %s adiado para %s às %02d:%02d", title, toDay(calendar[Calendar.DAY_OF_WEEK]), hour, minute, alarmId)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Toast.makeText(context, toastText, Toast.LENGTH_LONG).show()



            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle (
                            AlarmManager.RTC_WAKEUP,
                            calendar.timeInMillis,
                            alarmPendingIntent
                    )
            }

        }
        isStarted = true
    }

    fun cancelAlarm(context: Context) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, AlarmBroadcastReceiver::class.java)
        val alarmPendingIntent = PendingIntent.getBroadcast(
                context,
                alarmId,
                intent,
                0
        )
        alarmManager.cancel(alarmPendingIntent)
        isStarted = false
        val toastText = String.format(
                "Alarme das %02d:%02d com o título %s cancelado",
                hour,
                minute,
                title
        )
        Toast.makeText(context, toastText, Toast.LENGTH_SHORT).show()
        Log.i("cancel", toastText)
    }

    val recurringDaysText: String?
        get() {
            if (!isRecurring) {
                return null
            }
            var days = ""
            if (isMonday) {
                days += "Segunda "
            }
            if (isTuesday) {
                days += "Terça "
            }
            if (isWednesday) {
                days += "Quarta "
            }
            if (isThursday) {
                days += "Quinta "
            }
            if (isFriday) {
                days += "Sexta "
            }
            if (isSaturday) {
                days += "Sábado "
            }
            if (isSunday) {
                days += "Domingo "
            }
            return days
        }

}