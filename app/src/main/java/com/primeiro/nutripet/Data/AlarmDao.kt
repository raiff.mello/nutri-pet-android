package com.primeiro.nutripet.Data

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface AlarmDao {
    @Insert
    fun insert(alarm: Alarm?)

    @Query("DELETE FROM alarm_table")
    fun deleteAll()

    @Delete
    fun delete(alarm: Alarm?)

    @Query("SELECT * FROM alarm_table ORDER BY created ASC")
    fun getAlarms(): LiveData<List<Alarm>>

    @Query("SELECT * FROM alarm_table WHERE alarmId=:id")
    fun getAlarm(id: Int?): LiveData<List<Alarm>>

    @Update
    fun update(alarm: Alarm?)
}