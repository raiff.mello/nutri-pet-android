package com.primeiro.nutripet.Data

import android.app.Application
import androidx.lifecycle.LiveData
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class AlarmRepository(application: Application) {
    private val alarmDao: AlarmDao
    private val alarmsLiveData: LiveData<List<Alarm>>
    private val executor: Executor = Executors.newSingleThreadExecutor()

    fun insert(alarm: Alarm?) {
        AlarmDatabase.databaseWriteExecutor.execute { alarmDao.insert(alarm) }
    }

    fun update(alarm: Alarm?) {
        executor.execute(Runnable() {
            run() {
                AlarmDatabase.databaseWriteExecutor.execute { alarmDao.update(alarm) }
            }
        })
    }

    fun delete(alarm: Alarm?) {
        AlarmDatabase.databaseWriteExecutor.execute { alarmDao.delete(alarm) }
    }

    fun getAlarmsLiveData(): LiveData<List<Alarm>> {
        return alarmsLiveData
    }

    fun getAlarmLiveData(id: Int?): LiveData<List<Alarm>> {
        return alarmDao.getAlarm(id)
    }

    init {
        val db: AlarmDatabase? = AlarmDatabase.getDatabase(application)
        alarmDao = db!!.alarmDao()!!
        alarmsLiveData = alarmDao.getAlarms()
    }
}
