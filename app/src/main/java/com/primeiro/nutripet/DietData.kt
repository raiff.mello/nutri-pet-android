package com.primeiro.nutripet

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import java.io.File
import java.io.IOException


/*
This class is responsible for administering the application user registration information locally.
Internal storage with SharedPreferences was used here. Of the functions present, it is possible to
save data(in type String) necessary for registration, it is possible to specifically retrieve data
and there is also a function that excludes the registration data.
*/

class DietData(private val context: Context, private val petID: String?) {

    private val DIET_FILE = "data.diet"

    private val LUNCH_NAME = "LUNCH_NAME"
    private val LUNCH_TIME = "LUNCH_TIME"
    private val DINNER_NAME = "DINNER_NAME"
    private val DINNER_TIME = "DINNER_TIME"
    private val LUNCH_ALARM_ID = "LUNCH_ALARM_ID"
    private val DINNER_ALARM_ID = "DINNER_ALARM_ID"

    private val preferences: SharedPreferences
    private val editor: SharedPreferences.Editor


    fun SaveLunchName(data: String?){
        // Function to save the lunch name in the local file.
        editor.putString(LUNCH_NAME, data)
        editor.commit()
    }
    fun SaveLunchTime(data: String?){
        // Function to save the lunch time in the local file.
        editor.putString(LUNCH_TIME, data)
        editor.commit()
    }

    fun SaveDinnerName(data: String?){
        // Function to save the dinner name in the local file.
        editor.putString(DINNER_NAME, data)
        editor.commit()
    }

    fun SaveDinnerTime(data: String?){
        // Function to save the dinner time in the local file.
        editor.putString(DINNER_TIME, data)
        editor.commit()
    }

    fun SaveLunchAlarmId(data: Int) {
        editor.putInt(LUNCH_ALARM_ID, data)
        editor.commit()
    }

    fun SaveDinnerAlarmId(data: Int) {
        editor.putInt(DINNER_ALARM_ID, data)
        editor.commit()
    }

    fun RetrieveLunchName(): String? {
        // Function that returns the name of the lunch saved in the local file.
        return preferences.getString(LUNCH_NAME, "")
    }

    fun RetrieveLunchTime(): String? {
        // Function that returns the time of the lunch saved in the local file.
        return preferences.getString(LUNCH_TIME, "")
    }

    fun RetrieveDinnerName(): String? {
        // Function that returns the name of the lunch saved in the local file.
        return preferences.getString(DINNER_NAME, "")
    }

    fun RetrieveDinnerTime(): String? {
        // Function that returns the time of the lunch saved in the local file.
        return preferences.getString(DINNER_TIME, "")
    }

    fun RetrieveLunchAlarmId(): Int? {
        return preferences.getInt(LUNCH_ALARM_ID, 0)
    }

    fun RetrieveDinnerAlarmId(): Int? {
        return preferences.getInt(DINNER_ALARM_ID, 0)
    }

    fun DeleteDietData() {
        preferences.edit().clear().commit()
        File("/data/data/com.primeiro.nutripet/shared_prefs/data.diet" + petID + ".xml").delete()
    }

    init {
        // Function that initializes sharedpreferences
        preferences = context.getSharedPreferences(DIET_FILE + petID, 0)
        editor = preferences.edit()
    }
}