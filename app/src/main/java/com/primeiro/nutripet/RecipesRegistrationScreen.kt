package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_recipes_registration_screen.*

class RecipesRegistrationScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipes_registration_screen)
        bt_register_recipe.setText("Cadastrar Receita")

        val recipeData = RecipeData(applicationContext)

        val saveRecipe = arrayOf(edit_recipe_name, edit_ingredients, edit_preparation_mode)


        bt_register_recipe.setOnClickListener {
            if (saveRecipe[0].text.toString() == "" || saveRecipe[0].text.toString().contains(",") || saveRecipe[0].text.toString().trim() == "") {
                Toast.makeText(this, "Digite o nome da receita e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (saveRecipe[1].text.toString() == "" || saveRecipe[1].text.toString().contains(",") || saveRecipe[1].text.toString().trim() == "") {
                Toast.makeText(this, "Digite ao menos um ingrediente e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else if (saveRecipe[2].text.toString() == "" || saveRecipe[2].text.toString().contains(",") || saveRecipe[2].text.toString().trim() == "") {
                Toast.makeText(this, "Digite um modo de preparo e não adicione vírgulas", Toast.LENGTH_SHORT).show()
            } else {
                var recipename = recipeData.RecoverNameRecipe()
                recipename += "," + saveRecipe[0].text.toString()

                var ingredientes = recipeData.RecoverIngredients()
                ingredientes += "," + saveRecipe[1].text.toString()

                var preparation_mode = recipeData.RecoverPreparationMode()
                preparation_mode += "," + saveRecipe[2].text.toString()

                recipeData.SaveNameRecipe(recipename)
                recipeData.SaveIngredients(ingredientes)
                recipeData.SavePreparationMode(preparation_mode)
                Toast.makeText(this, "A receita foi cadastrada!", Toast.LENGTH_SHORT).show()

                var intent = Intent(this, ViewRecipes::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onBackPressed() {
        var intent = Intent(this, ViewRecipes::class.java)
        startActivity(intent)
        finish()
    }
}