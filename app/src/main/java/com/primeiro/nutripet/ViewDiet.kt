package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.primeiro.nutripet.Data.Alarm
import com.primeiro.nutripet.Data.AlarmRepository
import kotlinx.android.synthetic.main.activity_view_diet.*
import java.io.File

class ViewDiet : AppCompatActivity() {
    lateinit var alarmRepository: AlarmRepository
    var alarmDinner: Alarm? = null
    var alarmLunch: Alarm? = null
    var petID: String? = null
    var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_diet)

        intent.extras?.let {
            position = intent.getIntExtra("position", 0)
            petID = intent.getStringExtra("PET_ID")
            val managerDietData = DietData(applicationContext, petID)
            val managerSubstituteFoodData = SubstituteFoodData(applicationContext, petID)

            text_lunch.setText("Almoço: " + managerDietData.RetrieveLunchName())
            text_time_lunch.setText("Horário - Almoço: " + managerDietData.RetrieveLunchTime())

            text_dinner.setText("Janta: " + managerDietData.RetrieveDinnerName())
            text_time_dinner.setText("Horário - Janta: " + managerDietData.RetrieveDinnerTime())

            alarmRepository = AlarmRepository(application)


            alarmRepository.getAlarmLiveData(managerDietData.RetrieveLunchAlarmId())
                    .observe(this, Observer<List<Alarm>> { alarms ->
                        for (a in alarms) {
                            alarmLunch = a
                            if (a.isRecurring) {
                                checkbox_alarm_recurring_lunch.isChecked = true
                                checkbox_alarm_recurring_lunch_options.visibility = View.VISIBLE

                                if (a.isMonday) checkbox_alarm_lunch_mon.isChecked = true
                                if (a.isTuesday) checkbox_alarm_lunch_tue.isChecked = true
                                if (a.isWednesday) checkbox_alarm_lunch_wed.isChecked = true
                                if (a.isThursday) checkbox_alarm_lunch_thu.isChecked = true
                                if (a.isFriday) checkbox_alarm_lunch_fri.isChecked = true
                                if (a.isSaturday) checkbox_alarm_lunch_sat.isChecked = true
                                if (a.isSunday) checkbox_alarm_lunch_sun.isChecked = true
                            }
                        }
                    })


            alarmRepository.getAlarmLiveData(managerDietData.RetrieveDinnerAlarmId())
                    .observe(this, Observer<List<Alarm>> { alarms ->
                        for (a in alarms) {
                            alarmDinner = a
                            if (a.isRecurring) {
                                checkbox_alarm_recurring_dinner.isChecked = true
                                checkbox_alarm_recurring_dinner_options.visibility = View.VISIBLE

                                if (a.isMonday) checkbox_alarm_dinner_mon.isChecked = true
                                if (a.isTuesday) checkbox_alarm_dinner_tue.isChecked = true
                                if (a.isWednesday) checkbox_alarm_dinner_wed.isChecked = true
                                if (a.isThursday) checkbox_alarm_dinner_thu.isChecked = true
                                if (a.isFriday) checkbox_alarm_dinner_fri.isChecked = true
                                if (a.isSaturday) checkbox_alarm_dinner_sat.isChecked = true
                                if (a.isSunday) checkbox_alarm_dinner_sun.isChecked = true
                            }
                        }
                    })


            bt_delete_diet.setOnClickListener {
                dialogDeleteDiet(managerDietData, managerSubstituteFoodData, petID.toString())
            }

            button_back_view_diet.setOnClickListener {
                var intent = Intent(this, ViewPetsInfo::class.java)
                intent.putExtra("position", position)
                startActivity(intent)
                finish()
            }

            button_edit_diet_info.setOnClickListener {
                var intent = Intent(this, EditDiet::class.java)
                intent.putExtra("PET_ID", petID)
                startActivity(intent)
                finish()
            }


            var intent: Intent

            button_subtitute_food.setOnClickListener {
                val file = File("/data/data/com.primeiro.nutripet/shared_prefs/data.substituteFood" + petID + ".xml")
                if (file.exists()) {
                    intent = Intent(this, ViewSubstituteFood::class.java)
                    intent.putExtra("PET_ID", petID)
                    startActivity(intent)
                    finish()
                } else {
                    intent = Intent(this, RegisterSubstituteFood::class.java)
                    intent.putExtra("PET_ID", petID)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    fun dialogDeleteDiet(managerDietData: DietData, substituteFoodData: SubstituteFoodData, petID: String?) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Deseja deletar a dieta do pet?")
                .setCancelable(false)
                .setPositiveButton("Sim") { dialog, id ->
                    deleteAlarms()
                    managerDietData.DeleteDietData()
                    Toast.makeText(this, "Dieta deletada!", Toast.LENGTH_SHORT).show()
                    var intent = Intent(this, ViewPetsInfo::class.java)
                    intent.putExtra("position", position)
                    startActivity(intent)
                    finish()
                    File("/data/data/com.primeiro.nutripet/shared_prefs/data.diet" + petID + ".xml").delete()
                    substituteFoodData.DeleteSubstituteFood()
                    File("/data/data/com.primeiro.nutripet/shared_prefs/data.substituteFood" + petID + ".xml").delete()
                }
                .setNegativeButton("Não") { dialog, id ->
                    dialog.dismiss()
                }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun deleteAlarms() {
        if (alarmLunch != null) {
            if (alarmLunch!!.isStarted) alarmLunch!!.cancelAlarm(applicationContext)
            alarmRepository.delete(alarmLunch)
        }
        if (alarmDinner != null) {
            if (alarmDinner!!.isStarted) alarmDinner!!.cancelAlarm(applicationContext)
            alarmRepository.delete(alarmDinner)
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, ViewPetsInfo::class.java)
        Toast.makeText(this, "$position", Toast.LENGTH_SHORT).show()
        intent.putExtra("position", position)
        startActivity(intent)
        finish()
    }
}