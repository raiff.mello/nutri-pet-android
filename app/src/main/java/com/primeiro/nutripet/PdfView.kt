package com.primeiro.nutripet

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_pdf_view.*

class PdfView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_view)

        pdfView.fromAsset("Manual-Nutripet.pdf")
            .enableSwipe(true) /* allows to block changing pages using swipe*/
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .load()
    }
}