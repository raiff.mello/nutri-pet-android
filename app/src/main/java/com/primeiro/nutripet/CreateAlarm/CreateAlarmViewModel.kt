package com.primeiro.nutripet.CreateAlarm


import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.primeiro.nutripet.Data.Alarm
import com.primeiro.nutripet.Data.AlarmRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke
import kotlinx.coroutines.launch


class CreateAlarmViewModel(application: Application) : AndroidViewModel(application) {
    private val alarmRepository: AlarmRepository
    fun insert(alarm: Alarm?) {
        alarmRepository.insert(alarm)
    }

    fun update(alarm: Alarm?) {
        viewModelScope.launch(Dispatchers.IO) {
            alarmRepository.update(alarm)
        }
    }

    init {
        alarmRepository = AlarmRepository(application)
    }
}