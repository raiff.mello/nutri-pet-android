package com.primeiro.nutripet.CreateAlarm

import java.util.*


object DayUtil {
    @Throws(Exception::class)
    fun toDay(day: Int): String {
        when (day) {
            Calendar.SUNDAY -> return "Domingo"
            Calendar.MONDAY -> return "Segunda"
            Calendar.TUESDAY -> return "Terça"
            Calendar.WEDNESDAY -> return "Quarta"
            Calendar.THURSDAY -> return "Quinta"
            Calendar.FRIDAY -> return "Sexta"
            Calendar.SATURDAY -> return "Sábado"
        }
        throw Exception("Não foi possível localizar o dia")
    }
}