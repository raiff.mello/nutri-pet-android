package com.primeiro.nutripet

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import kotlinx.android.synthetic.main.activity_owner_registration_screen.*
import java.io.File


/*
Control class of the "owner registration screen" activity where it manages the functions and data
collection so that they are saved locally.
 */

class OwnerRegistrationScreen : AppCompatActivity() {
    var checkNumber = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_owner_registration_screen)

        val manage_owner_data = OwnerData(applicationContext)                                       // Variable that receives OwnerData;
        val text_field_data = arrayOf(view_name_owner, collect_email_owner, collect_phone_owner) // Owner information collected from the activity;
        val register_button = button_register_owner // Button to register the collected data;

        collect_phone_owner.doAfterTextChanged {
            checkNumber = collect_phone_owner.isDone
        }

        cb_terms.setOnCheckedChangeListener { buttonView, isChecked ->
            register_button.isEnabled = cb_terms.isChecked
        }

        tv_terms.setOnClickListener {
            val intent = Intent(this, PdfView::class.java)
            startActivity(intent)
        }

        register_button.setOnClickListener {
            val owner_name = text_field_data[0].text.toString()         // Owner name collected from the activity;
            val owner_mail = text_field_data[1].text.toString()         // Owner email collected from the activity;
            val owner_phone = text_field_data[2].text.toString()        // Owner phone collected from the activity;

            if (owner_name == "") {             // Checking empty name field;
                Toast.makeText(this, "Digite o seu nome", Toast.LENGTH_SHORT).show()
            } else if (owner_mail == "") {      // Checking empty email field;
                Toast.makeText(this, "Digite um email", Toast.LENGTH_SHORT).show()
            } else if (!verifyEmail(owner_mail)) {      // Checking empty email field;
                Toast.makeText(this, "Digite um email válido", Toast.LENGTH_SHORT).show()
            } else if (owner_phone == "") {      // Checking empty email field;
                Toast.makeText(this, "Digite um telefone", Toast.LENGTH_SHORT).show()
            } else if (!checkNumber) {     // Checking empty phone field;
                Toast.makeText(this, "Digite um telefone válido", Toast.LENGTH_SHORT).show()
            } else {
                manage_owner_data.ChangeStatus("menu")    // Changing control state of the login screen;
                manage_owner_data.SaveOwnerName(owner_name)         // Saving owner name in local file;
                manage_owner_data.SaveEmailOwner(owner_mail)        // Saving owner email in local file;
                manage_owner_data.SaveOwnerPhone(owner_phone)       // Saving owner phone in local file;
                Toast.makeText(this, "Dono cadastrado!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onBackPressed() {
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun verifyEmail(email: String):Boolean{
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        }
        return false;
    }

    fun openPDFDocument(context: Context, filename: String) {
        //Create PDF Intent
        val pdfFile = File(Environment.getExternalStorageDirectory().absolutePath + "/" + filename)
        val pdfIntent = Intent(Intent.ACTION_VIEW)
        pdfIntent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf")
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)

        //Create Viewer Intent
        val viewerIntent = Intent.createChooser(pdfIntent, "Open PDF")
        context.startActivity(viewerIntent)
    }
}