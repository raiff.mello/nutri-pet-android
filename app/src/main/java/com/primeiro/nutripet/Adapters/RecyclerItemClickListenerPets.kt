package com.primeiro.nutripet.Adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_pets.view.*


interface OnItemClickListenerPet {

    fun onItemClickedPet(position: Int, view: View)

}

fun RecyclerView.addOnItemClickListenerPet(onClickListener: OnItemClickListenerPet) {

    this.addOnChildAttachStateChangeListener(object :

        RecyclerView.OnChildAttachStateChangeListener {

        override fun onChildViewDetachedFromWindow(view: View) {

            view.bt_view_diet.setOnClickListener(null)

        }

        override fun onChildViewAttachedToWindow(view: View) {

            view.bt_view_diet.setOnClickListener({

                val holder = getChildViewHolder(view)

                onClickListener.onItemClickedPet(holder.adapterPosition, view)


            })

        }

    })

}