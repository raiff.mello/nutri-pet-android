package com.primeiro.nutripet.Adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_recipes.view.*


interface OnItemClickListenerRecipe {

    fun onItemClickedRecipe(position: Int, view: View)

}

fun RecyclerView.addOnItemClickListenerRecipe(onClickListener: OnItemClickListenerRecipe) {

    this.addOnChildAttachStateChangeListener(object :

            RecyclerView.OnChildAttachStateChangeListener {

        override fun onChildViewDetachedFromWindow(view: View) {

            view?.bt_view_recipe.setOnClickListener(null)

        }

        override fun onChildViewAttachedToWindow(view: View) {

            view?.bt_view_recipe.setOnClickListener({

                val holder = getChildViewHolder(view)

                onClickListener.onItemClickedRecipe(holder.adapterPosition, view)


            })

        }

    })

}