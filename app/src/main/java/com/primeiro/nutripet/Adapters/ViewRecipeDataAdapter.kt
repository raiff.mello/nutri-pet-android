package com.primeiro.nutripet.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.primeiro.nutripet.R
import com.primeiro.nutripet.RecipeData
import kotlinx.android.synthetic.main.list_recipes.view.*


class ViewRecipeDataAdapter(
    val recipes: MutableList<RecipeData.ViewRecipeData>): RecyclerView.Adapter<ViewRecipeDataAdapter.ViewRecipeDataViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewRecipeDataAdapter.ViewRecipeDataViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_recipes, parent, false)
        return ViewRecipeDataViewHolder(view)
    }

    override fun getItemCount(): Int = recipes.size

    override fun onBindViewHolder(holder: ViewRecipeDataAdapter.ViewRecipeDataViewHolder, position: Int) {
        holder.bind(recipes[position])
    }

    inner class ViewRecipeDataViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(recipe: RecipeData.ViewRecipeData){
            itemView.text_name_recipe.text = recipe.name
        }
    }
}