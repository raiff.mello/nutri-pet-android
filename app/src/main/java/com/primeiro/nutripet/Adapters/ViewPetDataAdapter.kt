package com.primeiro.nutripet.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.primeiro.nutripet.DataPet
import com.primeiro.nutripet.R
import kotlinx.android.synthetic.main.list_pets.view.*


class ViewPetDataAdapter(
    val pets: MutableList<DataPet.ViewPetData>): RecyclerView.Adapter<ViewPetDataAdapter.ViewPetDataViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPetDataAdapter.ViewPetDataViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_pets, parent, false)
        return ViewPetDataViewHolder(view)
    }

    override fun getItemCount(): Int = pets.size

    override fun onBindViewHolder(holder: ViewPetDataAdapter.ViewPetDataViewHolder, position: Int) {
        holder.bind(pets[position])
    }

    inner class ViewPetDataViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(pets: DataPet.ViewPetData){
            itemView.image_photograph.setImageResource(pets.photographpet)
            itemView.text_name_list.text = pets.name
            itemView.text_animal_list.text = pets.animal
            itemView.text_race_list.text = pets.race
            itemView.text_weight_list.text = pets.weight
            itemView.text_age_list.text = pets.age
        }
    }
}