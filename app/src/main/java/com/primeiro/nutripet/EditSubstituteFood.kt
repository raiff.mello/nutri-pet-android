package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_edit_substitute_food.*
import kotlinx.android.synthetic.main.activity_register_substitute_food.*
import kotlinx.android.synthetic.main.activity_register_substitute_food.register_substitute_lunch

class EditSubstituteFood : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_substitute_food)
        val petID = intent.getStringExtra("PET_ID")

        val manage_substitute_food_data = SubstituteFoodData(applicationContext, petID)    // Variable that receives SubstituteFoodData;         // Variable for the status of the registration button;

        edit_substitute_lunch.setText(manage_substitute_food_data.RetrieveSubstituteFoodLunch())
        edit_substitute_dinner.setText(manage_substitute_food_data.RetrieveSubstituteFoodDinner())

        button_edit_substitute_food.setOnClickListener {
            val edit_lunch = edit_substitute_lunch.text.toString()                  // Substitute Food Lunch collected from the activity;
            val edit_dinner = edit_substitute_dinner.text.toString()                // Substitute Food Dinner collected from the activity;

            if (edit_lunch == "") {
                Toast.makeText(this, "Digite um alimento substituto para o almoço", Toast.LENGTH_SHORT).show()
            }else if (edit_dinner == "") {
                Toast.makeText(this, "Digite um alimento substituto para o jantar", Toast.LENGTH_SHORT).show()
            }else {
                manage_substitute_food_data.ChangeStatusSubstituteFood("menu")    // Changing control state of the login screen;
                manage_substitute_food_data.SaveSubstituteFoodLunch(edit_lunch)                     // Saving edit lunch in local file;
                manage_substitute_food_data.SaveSubstituteFoodDinner(edit_dinner)                   // Saving edit dinner in local file;
                Toast.makeText(this, "Os alimentos substitutos foram editados!", Toast.LENGTH_SHORT).show()
                var intent = Intent(this, ViewSubstituteFood::class.java)
                intent.putExtra("PET_ID", petID)
                startActivity(intent)
                finish()
            }
        }

    }
    override fun onBackPressed() {
        val petID = intent.getStringExtra("pet")
        var intent = Intent(this, ViewSubstituteFood::class.java)
        intent.putExtra("PET_ID", petID)
        startActivity(intent)
        finish()
    }
}