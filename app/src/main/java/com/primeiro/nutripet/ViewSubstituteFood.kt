package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_register_substitute_food.*
import kotlinx.android.synthetic.main.activity_view_owner.*
import kotlinx.android.synthetic.main.activity_view_substitute_food.*
import java.io.File

class ViewSubstituteFood : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_substitute_food)

        val petID = intent.getStringExtra("PET_ID")

        val manage_substitute_food_data = SubstituteFoodData(applicationContext, petID)                    // Calling class of the substitute food registered data;

        view_substitute_food_lunch.setText("Almoço: ${manage_substitute_food_data.RetrieveSubstituteFoodLunch()}")        // Collecting the substitute lunch for textview;
        view_substitute_food_dinner.setText("Janta: ${manage_substitute_food_data.RetrieveSubstituteFoodDinner()}")        // Collecting the substitute dinner for textview;

        button_back_view_substitute_food.setOnClickListener {
            var intent = Intent(this, ViewDiet::class.java)
            intent.putExtra("PET_ID", petID)
            startActivity(intent)
            finish()
        }

        button_delete_substitute_food.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Deseja deletar os alimentos substitutos cadastrados?")
                .setCancelable(false)
                .setPositiveButton("Sim") { dialog, id ->
                    manage_substitute_food_data.DeleteSubstituteFood()
                    Toast.makeText(this, "Alimentos substitutos excluídos!", Toast.LENGTH_SHORT).show()
                    var intent = Intent(this, ViewDiet::class.java)
                    intent.putExtra("PET_ID", petID)
                    startActivity(intent)
                    finish()
                    File("/data/data/com.primeiro.nutripet/shared_prefs/data.substituteFood" + petID + ".xml").delete()
                }
                .setNegativeButton("Não") { dialog, id -> dialog.dismiss()
                }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        button_edit_substitute_food_info.setOnClickListener {
            var intent = Intent(this, EditSubstituteFood::class.java)
            intent.putExtra("PET_ID", petID)
            startActivity(intent)
            finish()
        }

    }
    override fun onBackPressed() {
        val petID = intent.getStringExtra("PET_ID")
        var intent = Intent(this, ViewDiet::class.java)
        intent.putExtra("PET_ID", petID)
        startActivity(intent)
        finish()
    }
}

