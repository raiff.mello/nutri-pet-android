package com.primeiro.nutripet

import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast

class RecipeData(private val context: Context) {

    private val ARCHIVERECIPE = "data.recipe"

    private val NAMERECIPE = "RECIPE"
    private val INGREDIENTS = "INGREDIENTS"
    private val PREPARATION_MODE = "PREPARATION_MODE"
    private val preferences: SharedPreferences
    private val editor: SharedPreferences.Editor

    fun SaveNameRecipe(data: String?){
        editor.putString(NAMERECIPE, data)
        editor.commit()
    }

    fun SaveIngredients(data: String?){
        editor.putString(INGREDIENTS, data)
        editor.commit()
    }

    fun SavePreparationMode(data: String?){
        editor.putString(PREPARATION_MODE, data)
        editor.commit()
    }


    fun RecoverNameRecipe(): String? {
        return preferences.getString(NAMERECIPE, "")
    }
    fun RecoverIngredients(): String? {
        return preferences.getString(INGREDIENTS, "")
    }
    fun RecoverPreparationMode(): String? {
        return preferences.getString(PREPARATION_MODE, "")
    }

    init {
        preferences = context.getSharedPreferences(ARCHIVERECIPE, 0)
        editor = preferences.edit()
    }

    fun DeleteRecipeRegistration(index: Int): Boolean {
        var name11 = RecoverNameRecipe().toString().split(",").toMutableList()
        var ingredients11 = RecoverIngredients().toString().split(",").toMutableList()
        var preparation_mode11 = RecoverPreparationMode().toString().split(",").toMutableList()

        name11.removeAt(index)
        ingredients11.removeAt(index)
        preparation_mode11.removeAt(index)

        var name = name11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
        var ingredients = ingredients11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")
        var preparation_mode = preparation_mode11.toString().removePrefix("[").removeSuffix("]").replace(" ", "")

        SaveNameRecipe("$name")
        SaveIngredients("$ingredients")
        SavePreparationMode("$preparation_mode")
        Toast.makeText(context, "Cadastro da receita Apagado com sucesso!", Toast.LENGTH_SHORT).show()
        return true
    }

    fun DeleteAllRecipes(){
        SaveNameRecipe("")
        SaveIngredients("")
        SavePreparationMode("")
    }

    data class ViewRecipeData(
            val name: String,
            val ingredients: String,
            val preparation_mode: String)

    class ViewRecipeDataBuilder{
        var name: String = ""
        var ingredients: String = ""
        var preparation_mode: String = ""

        fun build():ViewRecipeData = ViewRecipeData(name, ingredients, preparation_mode)
    }

    fun viewRecipeData(block: ViewRecipeDataBuilder.() -> Unit): ViewRecipeData = ViewRecipeDataBuilder().apply(block).build()

    fun addViewRecipeData(viewRecipeData: ViewRecipeData, retorno:MutableList<ViewRecipeData>): MutableList<ViewRecipeData> {
        var mut = retorno
        mut.add(viewRecipeData)
        return mut
    }

}