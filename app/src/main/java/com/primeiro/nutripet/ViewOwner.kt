package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_view_owner.*
import java.io.File


/*
Class responsible for showing the registered data about the user of the application.
 */


class ViewOwner : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_owner)

        val manage_owner_data = OwnerData(applicationContext)
        val manage_recipe_data = RecipeData(applicationContext)
        val manage_pet_data = DataPet(applicationContext)

        view_name_owner.setText("Nome: " + manage_owner_data.RetrieveOwnerName())           // Collecting the name of the owner for textview;
        view_email_owner.setText("Email: " + manage_owner_data.RetrieveOwnerEmail())        // Collecting the email of the owner for textview;
        view_phone_owner.setText("Telefone: " + manage_owner_data.RetrieveOwnerPhone())     // Collecting the phone of the owner for textview;

        bt_delete_owner.setOnClickListener{
            dialogDeleteOwner(manage_owner_data, manage_recipe_data, manage_pet_data)
        }
        button_back_view_owner.setOnClickListener {
            var intent = Intent(this, InitMenuScreen::class.java)
            startActivity(intent)
            finish()
        }
        button_edit_owner.setOnClickListener {
            var intent = Intent(this, EditOwner::class.java)
            startActivity(intent)
            finish()
        }
    }

    fun dialogDeleteOwner(managedOwnerData: OwnerData, managedRecipeData: RecipeData, managedPetData: DataPet) {  // Plotting a confirmation message to delete the owner's registration;
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Deseja deletar o dono cadastrado?")
            .setCancelable(false)
            .setPositiveButton("Sim") { dialog, id ->
                managedOwnerData.DeleteOwnerData()
                managedOwnerData.ChangeStatus("register")
                Toast.makeText(this, "Dono deletado!", LENGTH_SHORT).show()
                var intentMain = Intent(this, MainActivity::class.java)     // If confirmed, the app returns to the login screen;
                startActivity(intentMain)
                //deletions
                val size = managedPetData.RecoverNamePet().toString().split(",").toMutableList()
                for (k in 0..size.size+1){
                    if(File("/data/data/com.primeiro.nutripet/shared_prefs/data.substituteFood" + k + ".xml").exists()){
                        File("/data/data/com.primeiro.nutripet/shared_prefs/data.substituteFood" + k + ".xml").delete()
                    }
                    if (File("/data/data/com.primeiro.nutripet/shared_prefs/data.diet" + k + ".xml").exists()){
                        File("/data/data/com.primeiro.nutripet/shared_prefs/data.diet" + k + ".xml").delete()
                    }
                    if (File("/data/data/com.primeiro.nutripet/shared_prefs/data.weight" + k + ".xml").exists()){
                        File("/data/data/com.primeiro.nutripet/shared_prefs/data.weight" + k + ".xml").delete()
                    }
                }
                managedPetData.DeleteAllPets()
                managedRecipeData.DeleteAllRecipes()
                finish()
            }
            .setNegativeButton("Não") { dialog, id ->
                dialog.dismiss()
            }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
    override fun onBackPressed() {
        var intent = Intent(this, InitMenuScreen::class.java)
        startActivity(intent)
        finish()
    }
}