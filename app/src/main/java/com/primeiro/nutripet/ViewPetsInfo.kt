package com.primeiro.nutripet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.primeiro.nutripet.Data.Alarm
import com.primeiro.nutripet.Data.AlarmRepository
import kotlinx.android.synthetic.main.activity_view_owner.*
import kotlinx.android.synthetic.main.activity_view_pets_info.*
import kotlinx.android.synthetic.main.activity_view_substitute_food.*
import java.io.File


class ViewPetsInfo : AppCompatActivity() {
    lateinit var alarmRepository: AlarmRepository
    var alarmDinner: Alarm? = null
    var alarmLunch: Alarm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_pets_info)

        intent.extras?.let {
            val position = it.getInt("position")
            val pets = DataPet(applicationContext)

            var name1 = pets.RecoverNamePet().toString().split(",").toMutableList()
            var animal1 = pets.RecoverTypeAnimal().toString().split(",").toMutableList()
            var race1 = pets.RecoverRace().toString().split(",").toMutableList()
            var weight1 = pets.RecoverWeight().toString().split(",").toMutableList()
            var age1 = pets.RecoverAge().toString().split(",").toMutableList()
            var id1 = pets.RecoverIdPet().toString().split(",").toMutableList()

            val managerDietData = DietData(applicationContext, id1[position + 1])
            val managerSubstituteFood = SubstituteFoodData(applicationContext, id1[position + 1])
            alarmRepository = AlarmRepository(application)

            alarmRepository.getAlarmLiveData(managerDietData.RetrieveLunchAlarmId())
                    .observe(this, Observer<List<Alarm>> { alarms ->
                        for (alarm in alarms) {
                            alarmLunch = alarm
                        }
                    })

            alarmRepository.getAlarmLiveData(managerDietData.RetrieveDinnerAlarmId())
                    .observe(this, Observer<List<Alarm>> { alarms ->
                        for (alarm in alarms) {
                            alarmDinner = alarm
                        }
                    })

            view_pet_name.text = "Nome: " + name1[position+1]
            view_animal.text = "Animal: " + animal1[position+1]
            view_race.text = "Raça: " + race1[position+1]

            if (weight1[position+1].endsWith(".") && weight1[position+1].startsWith(".")){
                view_weight.text = "Peso: 0" + weight1[position+1] + "0 Kg"
            }else if(weight1[position+1].startsWith(".")){
                view_weight.text = "Peso: 0" + weight1[position+1] + " Kg"
            }else if(weight1[position+1].endsWith(".")) {
                view_weight.text = "Peso: " + weight1[position+1] + "0 Kg"
            }else{
                view_weight.text = "Peso: " + weight1[position+1] + " Kg"
            }
            view_age.text = "Idade: " + age1[position+1]

            bt_delete_pet.setOnClickListener {
                dialogDeletePet(pets, position, managerDietData, managerSubstituteFood)
            }


            bt_back.setOnClickListener {
                var intent = Intent(this, ViewPets::class.java)
                startActivity(intent)
                finish()
                finish()
            }

            var intent:Intent
            val file = File("/data/data/com.primeiro.nutripet/shared_prefs/data.diet" + id1[position + 1] + ".xml")
            if (file.exists())  {
                bt_register_diet.setText("VISUALIZAR DIETA")
                intent = Intent(this, ViewDiet::class.java)
            } else {
                intent = Intent(this, DietRegistrationScreen::class.java)
            }

            bt_register_diet.setOnClickListener {
                intent.putExtra("PET_ID", id1[position + 1])
                intent.putExtra("PET_NAME", name1[position + 1])
                intent.putExtra("position", position)
                startActivity(intent)
                finish()
                finish()
            }

            button_edit_pet_info.setOnClickListener {
                var intent = Intent(this, EditPetInfo::class.java)
                intent.putExtra("PET_ID", position)
                startActivity(intent)
                finish()
                finish()
            }

            bt_register_new_specific_weight.setOnClickListener {
                intent = Intent(this, RegisterSpecificWeight::class.java)
                intent.putExtra("PET_ID", id1[position + 1])
                startActivity(intent)
                finish()
                finish()
            }

            bt_view_specific_weight.setOnClickListener {
                intent = Intent(this, ViewSpecificWeight::class.java)
                intent.putExtra("PET_ID", id1[position + 1])
                startActivity(intent)
                finish()
                finish()
            }
        }
    }

    fun dialogDeletePet(pets: DataPet, position: Int, dietData: DietData, substituteFoodData: SubstituteFoodData) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Realmente desejas deletar o pet ?")
            .setCancelable(false)
            .setPositiveButton("Sim") { dialog, id ->
                var intent = Intent(this, ViewPets::class.java)
                startActivity(intent)
                finish()
                finish()
                pets.DeletePetRegistration(position + 1)
                dietData.DeleteDietData()
                substituteFoodData.DeleteSubstituteFood()
                deleteAlarms()
            }
            .setNegativeButton("Não") { dialog, id ->
                dialog.dismiss()
            }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun deleteAlarms() {
        if (alarmLunch != null) {
            if (alarmLunch!!.isStarted) alarmLunch!!.cancelAlarm(applicationContext)
            alarmRepository.delete(alarmLunch)
        }
        if (alarmDinner != null) {
            if (alarmDinner!!.isStarted) alarmDinner!!.cancelAlarm(applicationContext)
            alarmRepository.delete(alarmDinner)
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, ViewPets::class.java)
        startActivity(intent)
        finish()
        finish()
    }
}